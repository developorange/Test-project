<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'test-project');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '/F/Q|RjwZDI/Juxt<O$((|a]K=mcH7Wv,FgBlmM3TTwH8ND6XqH?}DscVA/{D)w(');
define('SECURE_AUTH_KEY',  'sVQzmpeP(vvU{XLX%I@0VRg.l&NIhgfV=sMd5jqC:q Xpu<@$5d#2m n-nGzt/fD');
define('LOGGED_IN_KEY',    '%}4l ElzP=j?Z,@=rT s!R:nhtzc%}zgB)XaH0}wi:kU1L/#_VB#] rECg-=3+0E');
define('NONCE_KEY',        'p|QS7~LV][D{{Xcdz{K>$s_42J*]Jjro[5|TDEt`U~^t&8vD_rJe:![Jih^%i86O');
define('AUTH_SALT',        '3Vm,/(ywB(xlE]kRBoViy7N5*Kt #7A5m`7t:-qaISJgX4JysU-Llfr-j7y,^WAY');
define('SECURE_AUTH_SALT', 'h*A+_Hqrdv8B%-Hz$<o/Z8`2ldqd`LMf3aO>4@]e.4b}-MsxSx*NEh;`SDy[)[zY');
define('LOGGED_IN_SALT',   ' oF>+$CBR9PMy:01~}.8aM9#=UiQ0ZnUSStr,B+ YQX1H0/,U8WQ MV;(Uhx<1pA');
define('NONCE_SALT',       'UZ/c7D|q6n42&|z;ttdhfhQHoUl71i2es[hm9R5g)M`#&.(PFY7ftneClQ;}apPV');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
